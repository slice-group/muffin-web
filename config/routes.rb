Rails.application.routes.draw do

  root to: 'frontend#index'

  get "services/software-development", to: "frontend#software_development", as: "software_development"
  get "services/web-design", to: "frontend#web_design", as: "web_design"
  get "about", to: "frontend#about", as: "about"

  get "portfolio/:category", to: "frontend#show_portfolio", as: "show_portfolio"
  get "downloads/:category", to: "frontend#show_downloads", as: "show_downloads"
  get "portfolio/:category/:id", to: "frontend#show_attachment", as: "show_portfolio_attachment"
  get "downloads/:category/:id", to: "frontend#show_attachment", as: "show_downloads_attachment"

  mount InyxContactUsRails::Engine, :at => '', as: 'messages'
  mount InyxCatalogRails::Engine, :at => '', as: 'catalog'
  mount InyxBlogRails::Engine, :at => '', as: 'post'
  
  devise_for :users, skip: [:registration]

  resources :admin, only: [:index]

  scope :admin do
  	resources :users do
  		collection do
	  		post '/delete', to: 'users#delete'
  		end
  	end
  end
end
