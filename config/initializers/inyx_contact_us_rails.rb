# Agregar datos de configuración
InyxContactUsRails.setup do |config|
	config.mailer_to = "hola@studiomuffin.com"
	config.mailer_from = "hola@studiomuffin.com"
	config.name_web = "Studio Muffin"
	# Activar o Desactivar la ruta especificada ("messages/new")
	config.messages_new = false
	#Route redirection after send
	config.redirection = "/#contact_us"


	# Agregar keys de google recaptcha
	Recaptcha.configure do |config|
	  config.public_key  = "6LePiwYTAAAAAMi7WYLwQvblZpr26lcbemehRA4Q"
	  config.private_key = "6LePiwYTAAAAAHCk4b0GGB4MzzLbK0fcMdkcZEwD"
	end
end