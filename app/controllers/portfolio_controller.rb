class PortfolioController < ApplicationController
	layout 'layouts/frontend/application'
  def index
  	Location.add(request.remote_ip)
    @posts = InyxBlogRails::Post.where(public: true).order("created_at DESC").limit(6)
  end
end