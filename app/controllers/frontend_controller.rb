class FrontendController < ApplicationController
	layout 'layouts/frontend/application'
  def index
  	Location.add(request.remote_ip)
    @posts = InyxBlogRails::Post.where(public: true).order("created_at DESC").limit(6)
    @items = InyxCatalogRails::Catalog.find_by_category("Clientes")
  end

  def show_portfolio
    if params[:category].include?("-")
  	  @title = params[:category].sub!('-', ' ').split.map(&:capitalize)*" "
    else
      @title = params[:category].capitalize
    end
    @text = text_portfolio(@title)
    @category = params[:category]
  	@catalogs = InyxCatalogRails::Catalog.where(category: @title, public: true);
  end

  def show_downloads
    if params[:category].include?("-")
      @title = params[:category].sub!('-', ' ').split.map(&:capitalize)*" "
    else
      @title = params[:category].capitalize
    end
    @text = text_portfolio(@title)
    @category = params[:category]
    @catalogs = InyxCatalogRails::Catalog.where(category: @title, public: true);
  end

  def show_attachment
  	if params[:category].include?("-")
      @title = params[:category].sub!('-', ' ').split.map(&:capitalize)*" "
    else
      @title = params[:category].capitalize
    end
    @catalog = InyxCatalogRails::Catalog.find(params[:id]);     
  end

  def about
    
  end

  private
    def text_portfolio(title)
      if title == "Branding"
        return "Diseñamos y asesoramos tu <span style='color:#ed6d58'>identidad corporativa</span> de manera efectiva, funcional y estética"
      elsif title == "Diseños Digitales"
        return "Elaboramos <span style='color:#ed6d58'>diseños funcionales</span> que te permitan acercarte a tus seguidores online"
      elsif title == "Diseños Impresos"
        return "Diseñamos piezas publicitarias reproducibles como <span style='color:#ed6d58'>extensiones de tu empresa</span> o producto"
      elsif title == "Diseños Web"
        return "Creamos tu <span style='color:#ed6d58'>sitio web</span> adaptado a tu imagen e identidad con tecnología responsive"
      elsif title == "Imprimibles"
        return "Sólo descarga, imprime y disfruta de estos diseños <span style='color:#ed6d58'>creados para ti</a>"
      elsif title == "Wallpapers"
        return "Decora y dale estilo a tus dispositivos con los diseños que te obsequia <span style='color:#ed6d58'>studio muffin</a>"
      end      
    end

end
