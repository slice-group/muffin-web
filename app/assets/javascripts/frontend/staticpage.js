function removeHash () { 
  history.pushState("", document.title, window.location.pathname + window.location.search);
}

$( document ).ready(function() {

	$('.top-nav').onePageNav({
    currentClass: 'current',
    changeHash: false,
    scrollSpeed: 500,
    scrollThreshold: 0.5,
    filter: ':not(.external)',
    easing: 'swing',
    begin: function() {
      removeHash();
      $("#front-navbar").removeClass("background-hide");
      $("#front-navbar").addClass("background-show");
    },
    end: function() {
      

      /*navbar*/
      if($('.top-nav .current').find('a').attr('href') != "#home") {
        $("#front-navbar").removeClass("background-hide");
        $("#front-navbar").addClass("background-show");
      } else {
        $("#front-navbar").removeClass("background-show");
        $("#front-navbar").addClass("background-hide");
      }
    },
    scrollChange: function($currentListItem) {

      /*navbar*/
      $("#front-navbar").removeClass("background-hide");
      $("#front-navbar").addClass("background-show");

      if($('.top-nav .current').find('a').attr('href') == "#home") {
        $("#front-navbar").removeClass("background-show");
        $("#front-navbar").addClass("background-hide");
      }

      /*services*/
      if($('.top-nav .current').find('a').attr('href') == "#services") {

        $("#service-1").addClass("effect-left");
        $("#service-2").addClass("effect-left");
      }

      if($('.top-nav .current').find('a').attr('href') == "#services") {
        $("#service-3").addClass("effect-right");
        $("#service-4").addClass("effect-right");
      }

      /*about*/
      if($('.top-nav .current').find('a').attr('href') == "#about") {
        $("#about").addClass("effect_about");
      }

      /*portfolio*/
      if($('.top-nav .current').find('a').attr('href') == "#portfolio") {
        $("#container-portfolio").addClass("effect_portfolio");
      }

      /*downloads*/
      if($('.top-nav .current').find('a').attr('href') == "#downloads") {
        $("#container-downloads").addClass("effect_downloads");
      } 		  

      /*contact*/
      if($('.top-nav .current').find('a').attr('href') == "#contact_us") {
        $("form").addClass("effect_form");
        $(".contact-info").addClass("effect_info");
      }   
    }
  });
});



